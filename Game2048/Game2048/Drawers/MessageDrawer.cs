﻿using System;

namespace Game2048
{
    public class MessageDrawer : DrawerBase
    {
        public void Draw(string message, ConsoleColor color = ConsoleColor.White)
        {
            Prepare();

            if (string.IsNullOrEmpty(message))
            {
                Write(new string(' ', Console.WindowWidth));
            }
            else
            {
                Write(message, color);
            }
        }
    }
}