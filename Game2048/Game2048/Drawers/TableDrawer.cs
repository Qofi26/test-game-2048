﻿using System;

namespace Game2048
{
    public class TableDrawer : DrawerBase
    {
        private readonly int _tableSize;
        private readonly int _maxValueLength;

        private readonly ConsoleColor   _defaultColor;
        private readonly ConsoleColor   _tableColor;
        private readonly ConsoleColor[] _colors;

        private const int MIN_CELL_SIZE = 4;

        public TableDrawer(int tableSize, int maxValue)
        {
            _tableSize      = tableSize;
            _maxValueLength = Math.Max(MIN_CELL_SIZE, $"{Math.Pow(2, maxValue)}".Length);
            _tableColor     = ConsoleColor.White;
            _defaultColor   = ConsoleColor.DarkGray;
            _colors = new[]
            {
                ConsoleColor.DarkGray, //0

                ConsoleColor.Green, //1 (2)
                ConsoleColor.Blue, //2 (4)
                ConsoleColor.Red, //3 (8)
                ConsoleColor.Yellow, //4 (16)
                ConsoleColor.Magenta, //5 (32)
                ConsoleColor.Cyan, //6 (64)

                ConsoleColor.DarkGreen, //7 (128)
                ConsoleColor.DarkBlue, //8 (256)
                ConsoleColor.DarkRed, //9 (512)
                ConsoleColor.DarkYellow, //10 (1024)
                ConsoleColor.DarkMagenta, //11 (2048)
                ConsoleColor.DarkCyan, //12 (4096)
            };
        }

        public void DrawTable(int[,] table)
        {
            Prepare();

            DrawLineSeparator();

            for (int i = 0; i < _tableSize; i++)
            {
                Write("|", _tableColor);
                for (int j = 0; j < _tableSize; j++)
                {
                    DrawCell(table[i, j]);
                    Write("|", _tableColor);
                }

                DrawLineSeparator();
            }

            WriteLine();
        }

        private void DrawLineSeparator()
        {
            WriteLine();
            Write("|", _tableColor);
            for (int i = 0; i < _tableSize; i++)
            {
                Write(new string('-', _maxValueLength) + "|", _tableColor);
            }

            WriteLine();
        }

        private void DrawCell(int value)
        {
            var emptyColor  = _colors[0];
            var numberColor = value >= _colors.Length ? _defaultColor : _colors[value];

            if (value == 0)
            {
                Write(new string('·', _maxValueLength), emptyColor);
            }
            else
            {
                var str = $"{Math.Pow(2, value)}";
                Write(new string('·', _maxValueLength - str.Length), emptyColor);
                Write(str, numberColor);
            }
        }
    }
}