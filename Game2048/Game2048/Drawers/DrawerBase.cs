﻿using System;

namespace Game2048
{
    public abstract class DrawerBase
    {
        private int  _startTop;
        private int  _startLeft;
        private bool _isSetuped;

        protected static void Write(string value, ConsoleColor color = ConsoleColor.White)
        {
            Console.ForegroundColor = color;
            Console.Write(value);
        }

        protected static void WriteLine(string value = null, ConsoleColor color = ConsoleColor.White)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(value);
        }

        protected void Prepare()
        {
            if (_isSetuped == false)
            {
                _startTop  = Console.CursorTop;
                _startLeft = Console.CursorLeft;
                _isSetuped = true;
            }

            Console.SetCursorPosition(_startLeft, _startTop);
        }
    }
}