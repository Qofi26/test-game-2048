﻿using System;

namespace Game2048
{
    public class UIDrawer : DrawerBase
    {
        private readonly ConsoleColor _uiColor;
        private readonly string       _maxValueTitle;

        private const string TITLE       = "=== 2048 ===";
        private const string INSTRUCTION = "Use ↑ ↓ → ← for moving. R - restart, Q - quit";

        public UIDrawer(int maxValue)
        {
            _maxValueTitle = $"Move the numbers to get {Math.Pow(2, maxValue)}";
            _uiColor       = ConsoleColor.Green;
        }

        public void Draw(int score, int bestScore)
        {
            Prepare();

            WriteLine(TITLE, _uiColor);
            WriteLine($" Score: {score,-11}", _uiColor);
            WriteLine($" Best: {bestScore,-11}", _uiColor);
            WriteLine();
            WriteLine(_maxValueTitle, ConsoleColor.Yellow);
            WriteLine(INSTRUCTION, ConsoleColor.Yellow);
        }
    }
}