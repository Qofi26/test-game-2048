﻿using System.IO;
using System.Reflection;

namespace Game2048
{
    public class DataProvider
    {
        private readonly string _currentDir;

        public DataProvider()
        {
            _currentDir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        }

        public void Save(string name, string value)
        {
            var path = Path.Combine(_currentDir, name);

            File.WriteAllText(path, value);
        }

        public string Load(string name)
        {
            var path = Path.Combine(_currentDir, name);

            if (File.Exists(path) == false)
            {
                return null;
            }

            return File.ReadAllText(path);
        }
    }
}