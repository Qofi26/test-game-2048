﻿using System;

namespace Game2048
{
    public static class TableController
    {
        public static (int[,] table, int score, int maxValue, bool isMoved) Move(int[,] tableRef, MoveAction action)
        {
            var table       = (int[,]) tableRef.Clone();
            var tableSize   = table.GetLength(0);
            var score       = 0;
            var maxValue    = 0;
            var leftOrUp    = action is MoveAction.Left or MoveAction.Up;
            var leftOrRight = action is MoveAction.Left or MoveAction.Right;
            var isMoved     = false;

            for (int i = 0; i < tableSize; i++)
            {
                var ignored = -1;

                for (var j = leftOrUp ? 1 : tableSize - 2; leftOrUp ? j < tableSize : j >= 0; j += leftOrUp ? 1 : -1)
                {
                    ref var currentCell = ref GetCurrentCell(i, j);

                    if (currentCell == 0)
                    {
                        continue;
                    }

                    var newPosition = -1;

                    for (var k = leftOrUp ? j - 1 : j + 1; leftOrUp ? k >= 0 : k < tableSize; k += leftOrUp ? -1 : 1)
                    {
                        ref var nextCell = ref GetNextCell(i, k);

                        if (nextCell == 0)
                        {
                            newPosition = k;
                        }
                        else if (ignored != k && currentCell == nextCell)
                        {
                            nextCell    += 1;
                            currentCell =  0;
                            ignored     =  k;
                            score       += (int) Math.Pow(2, nextCell);
                            isMoved     =  true;
                            if (nextCell >= maxValue)
                            {
                                maxValue = nextCell;
                            }

                            break;
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (newPosition != -1)
                    {
                        MoveToPosition(i, newPosition, ref currentCell);
                    }
                }
            }

            #region Local

            ref int GetCurrentCell(int i, int j)
            {
                if (leftOrRight)
                {
                    return ref table[i, j];
                }

                return ref table[j, i];
            }

            ref int GetNextCell(int i, int k)
            {
                if (leftOrRight)
                {
                    return ref table[i, k];
                }

                return ref table[k, i];
            }

            void MoveToPosition(int i, int position, ref int currentCell)
            {
                if (leftOrRight)
                {
                    table[i, position] = currentCell;
                }
                else
                {
                    table[position, i] = currentCell;
                }

                currentCell = 0;
                isMoved     = true;
            }

            #endregion

            return (table, score, maxValue, isMoved);
        }
    }
}