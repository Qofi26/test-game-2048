﻿using System;
using System.Collections.Generic;

namespace Game2048
{
    public class GameController
    {
        private readonly DataProvider  _dataProvider;
        private readonly UIDrawer      _uiDrawer;
        private readonly TableDrawer   _tableDrawer;
        private readonly MessageDrawer _messageDrawer;
        private readonly int           _tableSize;
        private readonly int           _targetMaxValue;

        private int[,] _table;
        private int    _currentScore;
        private int    _bestScore;
        private int    _maxValue;
        private Random _random;

        private const int MAX_VALUE = 30;

        private const int CHANCE_RANDOM_2 = 15;

        private readonly Dictionary<MoveAction, (int[,], int score, int maxValue, bool isMoved)> _tableVariants = new();

        public GameController(DataProvider dataProvider, int tableSize = 4, int maxValue = 11)
        {
            maxValue = Math.Min(MAX_VALUE, maxValue);

            _dataProvider   = dataProvider;
            _uiDrawer       = new UIDrawer(maxValue);
            _tableDrawer    = new TableDrawer(tableSize, maxValue);
            _messageDrawer  = new MessageDrawer();
            _tableSize      = tableSize;
            _targetMaxValue = maxValue;

            _bestScore = LoadBestScore();
        }

        public void Gameplay()
        {
            _random       = new Random();
            _table        = new int[_tableSize, _tableSize];
            _currentScore = 0;
            _maxValue     = 0;

            _tableVariants.Clear();

            AddNewValue();

            while (true)
            {
                switch (GetInput())
                {
                    case ConsoleKey.LeftArrow:
                        MoveTable(MoveAction.Left);
                        break;
                    case ConsoleKey.RightArrow:
                        MoveTable(MoveAction.Right);
                        break;
                    case ConsoleKey.DownArrow:
                        MoveTable(MoveAction.Down);
                        break;
                    case ConsoleKey.UpArrow:
                        MoveTable(MoveAction.Up);
                        break;
                    case ConsoleKey.R:
                        if (WaitConfirm("Restart game? (Y / N)"))
                        {
                            Gameplay();
                            return;
                        }

                        break;
                    case ConsoleKey.Q:
                        if (WaitConfirm("Quit game? (Y / N)"))
                        {
                            return;
                        }

                        break;
                }

                if (_maxValue >= _targetMaxValue)
                {
                    if (WaitConfirm("You won!!! Restart or Quit? (R / Q)", ConsoleKey.R, ConsoleKey.Q))
                    {
                        Gameplay();
                    }

                    return;
                }

                if (CheckLose())
                {
                    if (WaitConfirm("You lost! Restart or Quit? (R / Q)", ConsoleKey.R, ConsoleKey.Q))
                    {
                        Gameplay();
                    }

                    return;
                }
            }
        }

        #region Private API

        private void DrawGame()
        {
            _uiDrawer.Draw(_currentScore, _bestScore);
            _tableDrawer.DrawTable(_table);
        }

        private void MoveTable(MoveAction action)
        {
            var (table, score, maxValue, isMoved) = _tableVariants[action];

            if (isMoved == false)
            {
                return;
            }

            _currentScore += score;
            if (_currentScore >= _bestScore)
            {
                _bestScore = _currentScore;
                SaveBestScore(_bestScore);
            }

            if (maxValue > _maxValue)
            {
                _maxValue = maxValue;
            }

            _table = table;

            AddNewValue();
        }

        private void AddNewValue()
        {
            var value = _random.Next(0, 100);

            value = value <= CHANCE_RANDOM_2 ? 2 : 1;

            var emptyCells = new List<(int row, int column)>();

            for (int i = 0; i < _tableSize; i++)
            {
                for (int j = 0; j < _tableSize; j++)
                {
                    if (_table[i, j] == 0)
                    {
                        emptyCells.Add((i, j));
                    }
                }
            }

            if (emptyCells.Count == 0)
            {
                return;
            }

            var cellIndex = _random.Next(0, emptyCells.Count);
            var (row, column)   = emptyCells[cellIndex];
            _table[row, column] = value;

            CalculateTableVariants();
            DrawGame();
        }

        private void CalculateTableVariants()
        {
            _tableVariants[MoveAction.Up]    = TableController.Move(_table, MoveAction.Up);
            _tableVariants[MoveAction.Right] = TableController.Move(_table, MoveAction.Right);
            _tableVariants[MoveAction.Down]  = TableController.Move(_table, MoveAction.Down);
            _tableVariants[MoveAction.Left]  = TableController.Move(_table, MoveAction.Left);
        }

        private bool CheckLose()
        {
            return (_tableVariants[MoveAction.Up].isMoved ||
                    _tableVariants[MoveAction.Right].isMoved ||
                    _tableVariants[MoveAction.Down].isMoved ||
                    _tableVariants[MoveAction.Left].isMoved) == false;
        }

        private ConsoleKey GetInput()
        {
            var top  = Console.CursorTop;
            var left = Console.CursorLeft;
            var key  = Console.ReadKey().Key;
            Console.SetCursorPosition(left, top);
            Console.Write(" ");
            Console.SetCursorPosition(left, top);
            return key;
        }

        private bool WaitConfirm(string message, ConsoleKey positive = ConsoleKey.Y, ConsoleKey negative = ConsoleKey.N)
        {
            _messageDrawer.Draw(message);

            var key = GetInput();
            _messageDrawer.Draw(null);
            if (key == positive)
            {
                return true;
            }

            if (key == negative)
            {
                return false;
            }

            return WaitConfirm(message, positive, negative);
        }

        private void SaveBestScore(int value)
        {
            _dataProvider.Save("record", value.ToString());
        }

        private int LoadBestScore()
        {
            var save = _dataProvider.Load("record");
            return int.TryParse(save, out var score) ? score : 0;
        }

        #endregion
    }
}