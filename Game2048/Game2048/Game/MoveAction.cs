﻿namespace Game2048
{
    public enum MoveAction
    {
        Up    = 0,
        Down  = 1,
        Left  = 2,
        Right = 3
    }
}