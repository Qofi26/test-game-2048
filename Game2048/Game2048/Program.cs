﻿using System;
using System.Text.Json;

namespace Game2048
{
    class Program
    {
        static void Main(string[] args)
        {
            Play();
        }

        private static void Play()
        {
            var dataProvider = new DataProvider();

            var config = ReadConfiguration(dataProvider);

            if (config == null)
            {
                return;
            }

            const int table_size = 4;

            var game = new GameController(dataProvider, table_size, config.WinValue);

            Console.CursorVisible = false;
            Console.Clear();
            game.Gameplay();
        }

        private static GameConfiguration ReadConfiguration(DataProvider provider)
        {
            const string name = "config.json";
            try
            {
                GameConfiguration config;

                var json = provider.Load(name);

                if (string.IsNullOrEmpty(json))
                {
                    config = new GameConfiguration();
                    json   = JsonSerializer.Serialize(config, new JsonSerializerOptions {WriteIndented = true});
                    provider.Save(name, json);
                }
                else
                {
                    config = JsonSerializer.Deserialize<GameConfiguration>(json);
                }

                return config;
            }
            catch
            {
                Console.WriteLine($"Invalid configuration file: {name}");
                return null;
            }
        }
    }
}